//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"


class PictureFeedCollectionViewController: UICollectionViewController {

    @IBOutlet var imageCollectionView: UICollectionView!
    
    private var hits = [Image]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        downloadJson()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        imageCollectionView.register(UINib(nibName: "ViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return hits.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ViewCollectionViewCell else {return UICollectionViewCell()}
        let row = hits[indexPath.row]
        if let previewURL = URL(string: row.previewURL) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: previewURL)
                if let data = data {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.cellImageView.image = image
                    }
                }
            }
        }
        // Configure the cell
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
     }
    */
    
    func downloadJson(){
        /*
        let session = URLSession.shared.dataTask(with: URLRequest(url: url!)) {(data, response, error) in
        if let httpResponse = response as? HTTPURLResponse {
            if(httpResponse.statusCode != 200) {
                //ERROR
            }
        }
        if let myData = data {
            if let json = try? JSONSerialization.jsonObject(with: myData, options: []) as! Dictionary<String,Any> {
                
                //parsing
                if let statusCode = json["totalHits"] as? String {
                    if(statusCode == "500") {
                        if let downloadedImages = json["hits"] as? Array<Dictionary<String,Any>> {
                            self.images = downloadedImages
                            DispatchQueue.main.async {
                                self.imageCollectionView.reloadData()
                            }
                        } else {
                            
                        }
                    }
                } else {
                    
                }
            } else {
                print("Error")
            }
        }*/
        let url = URL(string: "https://pixabay.com/api/?key="+ApiDetails.apiKey+"&q=flower")
        
        guard let downloadURL = url else { return }
        URLSession.shared.dataTask(with: downloadURL) {data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("Something is wrong")
                return
            }
            print("Downloaded")
            do
            {
                let decoder = JSONDecoder()
                let downloadedImages = try decoder.decode(Hits.self, from: data)
                self.hits = downloadedImages.hits
                DispatchQueue.main.async {
                    self.imageCollectionView.reloadData()
                }
            } catch {
                print("Something wrong after downloaded")
                print("\(error)")
            }
        }.resume()
    }
}
