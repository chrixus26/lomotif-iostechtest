//
//  ViewCollectionViewCell.swift
//  iOSTechnicalTest
//
//  Created by Chrissandro on 18/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit

class ViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
